resource "aws_cloudwatch_log_group" "loggroup" {
  name = "${var.name}/log"

  tags = {
    Environment = "production"
    Application = "${var.name}"
  }
}