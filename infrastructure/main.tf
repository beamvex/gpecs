# Specify the Terraform version
terraform {
  #required_version = ">= 0.12.0"

  # Specify the backend configuration for Terraform's state storage
  backend "s3" {
    bucket         = "${var.name}-tfstate-bucket"
    key            = "terraform.tfstate"
    region         = "eu-west-2"
    encrypt        = true
    dynamodb_table = "${var.name}-lock-table"
  }
}