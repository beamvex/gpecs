resource "aws_vpc" "awnet" {
  cidr_block = "10.20.76.0/24"
  enable_dns_hostnames = true
  tags = {
    Name = "${var.name}-awnet"
  }
}

resource "aws_subnet" "alpha" {
  vpc_id            =  aws_vpc.awnet.id
  availability_zone = "${var.az}"
  cidr_block        = "${var.cidr_block}"
  
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.name}-private-subnet"
  }
}

resource "aws_internet_gateway" "aw-igw" {
  vpc_id = aws_vpc.awnet.id

  tags = {
    Name = "${var.name}-igw"
  }
}

resource "aws_route_table" "aw-rtable" {
  vpc_id = aws_vpc.awnet.id
  

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.aw-igw.id
  }

  tags = {
    Name = "${var.name}-rtable"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.alpha.id
  route_table_id = aws_route_table.aw-rtable.id
}

resource "aws_main_route_table_association" "a" {
  vpc_id         = aws_vpc.awnet.id
  route_table_id = aws_route_table.aw-rtable.id
}

resource "aws_security_group" "allow_all_out" {
  name        = "${var.name}-allow_all_out"
  description = "Allow All outbound traffic"
  vpc_id      = aws_vpc.awnet.id

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    self = true
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.name}-allow_all_out"
  }
}