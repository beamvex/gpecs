# ECS Task Definition
resource "aws_ecs_task_definition" "awtask" {
  family                   = "${var.name}-awtask-family"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn = aws_iam_role.ecs_task_role.arn
  cpu                      = "256"
  memory                   = "512"

  container_definitions = jsonencode([
    {
      name  = "awcontainer",
      image = "animalmutha76/ssak:slim",
      command= [
            "bash", "-c", "echo hello"
         ],
      mountPoints = [
          {
              containerPath="/chrome-data",
              sourceVolume="${var.name}-chromedata"
          },
          {
              containerPath="/output",
              sourceVolume="${var.name}-outputdata"
          }
      ],
      logConfiguration = {
          logDriver= "awslogs",
          options = {
            awslogs-group="${var.name}/log",
            awslogs-region="${var.region}",
            awslogs-stream-prefix="ecs"
          }
        }
        environment= [
        {
          name="STACK" 
          value="${var.name}"
        }
      ]
    },
  ])

   volume {
    name      = "${var.name}-chromedata"
    efs_volume_configuration {
      file_system_id = aws_efs_file_system.chromedata.id
      root_directory = "/"
    }
  }
  volume {
    name      = "${var.name}-outputdata"
    efs_volume_configuration {
      file_system_id = aws_efs_file_system.outputdata.id
      root_directory = "/"
    }
  }
}