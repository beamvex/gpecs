output "subnet" {
    value = aws_subnet.alpha.id
}

output "securitygroup" {
    value = aws_security_group.allow_all_out.id
}