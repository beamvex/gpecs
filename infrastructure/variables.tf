variable "name" {
  type = string
}

variable "cidr_block" {
    type = string
}

variable "az" {
    type = string
}

variable "region" {
    type = string
}