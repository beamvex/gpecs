resource "aws_efs_file_system" "chromedata" {
  tags = {
    Name = "${var.name}-chromedata"    
  }
}

resource "aws_efs_mount_target" "mount" {
  file_system_id = aws_efs_file_system.chromedata.id
  subnet_id      = aws_subnet.alpha.id
  security_groups = [aws_security_group.allow_all_out.id]
  
}

resource "aws_efs_file_system" "outputdata" {
  tags = {
    Name = "${var.name}-outputdata"
  }
}

resource "aws_efs_mount_target" "mountod" {
  file_system_id = aws_efs_file_system.outputdata.id
  subnet_id      = aws_subnet.alpha.id
  security_groups = [aws_security_group.allow_all_out.id]  
}