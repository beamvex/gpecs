resource "aws_scheduler_schedule" "awcron" {
  name       = "aw-schedule"
  group_name = "default"

  flexible_time_window {
    mode = "OFF"
  }

  schedule_expression = "cron(32 8,14,18 * * ? *)"
  state               = "ENABLED"

  target {
    arn = aws_ecs_cluster.articlewriter.arn
    # role that allows scheduler to start the task (explained later)
    role_arn = aws_iam_role.scheduler.arn

    ecs_parameters {
      # trimming the revision suffix here so that schedule always uses latest revision
      task_definition_arn = trimsuffix(aws_ecs_task_definition.awtask.arn, ":${aws_ecs_task_definition.awtask.revision}")
      launch_type         = "FARGATE"

      network_configuration {
        assign_public_ip = true
        security_groups  = [aws_security_group.allow_all_out.id]
        subnets          = [aws_subnet.alpha.id]
      }
    }

    retry_policy {
      maximum_event_age_in_seconds = 300
      maximum_retry_attempts       = 1
    }
  }
}

resource "aws_scheduler_schedule" "awcron2" {
  name       = "aw-schedule-2"
  group_name = "default"

  flexible_time_window {
    mode = "OFF"
  }

  schedule_expression = "cron(29 10,16 * * ? *)"
  state               = "ENABLED"

  target {
    arn = aws_ecs_cluster.articlewriter.arn
    # role that allows scheduler to start the task (explained later)
    role_arn = aws_iam_role.scheduler.arn

    ecs_parameters {
      # trimming the revision suffix here so that schedule always uses latest revision
      task_definition_arn = trimsuffix(aws_ecs_task_definition.awtask.arn, ":${aws_ecs_task_definition.awtask.revision}")
      launch_type         = "FARGATE"

      network_configuration {
        assign_public_ip = true
        security_groups  = [aws_security_group.allow_all_out.id]
        subnets          = [aws_subnet.alpha.id]
      }
    }

    retry_policy {
      maximum_event_age_in_seconds = 300
      maximum_retry_attempts       = 1
    }
  }
}

resource "aws_scheduler_schedule" "awcron3" {
  name       = "aw-schedule-3"
  group_name = "default"

  flexible_time_window {
    mode = "OFF"
  }

  schedule_expression = "cron(35 12,20 * * ? *)"
  state               = "ENABLED"

  target {
    arn = aws_ecs_cluster.articlewriter.arn
    # role that allows scheduler to start the task (explained later)
    role_arn = aws_iam_role.scheduler.arn

    ecs_parameters {
      # trimming the revision suffix here so that schedule always uses latest revision
      task_definition_arn = trimsuffix(aws_ecs_task_definition.awtask.arn, ":${aws_ecs_task_definition.awtask.revision}")
      launch_type         = "FARGATE"

      network_configuration {
        assign_public_ip = true
        security_groups  = [aws_security_group.allow_all_out.id]
        subnets          = [aws_subnet.alpha.id]
      }
    }

    retry_policy {
      maximum_event_age_in_seconds = 300
      maximum_retry_attempts       = 1
    }
  }
}