#!/bin/bash
eval $(aws configure export-credentials --format env)

docker run \
    -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} \
    -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} \
    -e AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN} \
    -e AWS_DEFAULT_REGION=eu-west-2 \
    -v $(pwd):/terra/ \
    -it --rm --name terra \
    animalmutha76/ssak:slim \
    bash -c "cd terra;
    terragrunt init;
    terragrunt plan -out plan1;
    terragrunt apply plan1"