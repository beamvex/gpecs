# terragrunt.hcl

terraform {
  source = "./infrastructure"
}

generate "main" {
  path      = "main.tf"
  if_exists = "overwrite"
  contents = <<EOF
terraform {
  backend "s3" {
    bucket         = "generalperpose-terraform-state"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "eu-west-2"
    encrypt        = true
    dynamodb_table = "generalperpose-lock-table"
  }
}
EOF
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite"
  contents = <<EOF
provider "aws" {
  region = "eu-west-2"
}
EOF
}

# Inputs to pass to the Terraform module
inputs = {
  name    = "generalperpose"
  region  = "eu-west-2"
  az  = "eu-west-2a"
  cidr_block = "10.20.76.0/27"
  # ...other variables
}



